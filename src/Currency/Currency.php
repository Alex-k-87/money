<?php

namespace App\Currency;

final class Currency
{
    const CURRENCY_USD = 'USD';
    const CURRENCY_EUR = 'EUR';
        private $isoCode;
        private $currencies = [
        self::CURRENCY_EUR,
        self::CURRENCY_USD
    ];

    public function __construct($isoCode)
    {
        $this->setIsoCode($isoCode);
    }

    /**
     * @return mixed
     */
    public function getIsoCode()
    {
        return $this->isoCode;
    }

    /**
     * @param mixed $isoCode
     */
    private function setIsoCode($isoCode)
    {
        if (!in_array($isoCode, $this->currencies)) {
            throw new \InvalidArgumentException('Invalid Currency');
        }
        $this->isoCode = $isoCode;
    }

    public function equals(Currency $currency)
    {
        if ($this->isoCode === $currency->getIsoCode()) {
            return true;
        }
        return false;
    }

}