<?php


namespace App\Money;


use App\Currency\Currency;
use http\Exception\InvalidArgumentException;

final class Money
{
   private $amount;

    /** @var Currency */
   private $currency;

    public function __construct($amount, Currency $currency)
    {
        $this->setAmount($amount);
        $this->setCurrency($currency);
    }

    public function getAmount()
    {
        return $this->amount;
    }

    private function setAmount($amount): void
    {
        if ($amount<=0) {
            throw new \InvalidArgumentException('Invalid argument');
        }
        $this->amount = $amount;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    private function setCurrency(Currency $currency): void
    {
        $this->currency = $currency;
    }

    public function equals(Money $money)
    {
        if ($this->amount !== $money->getAmount() || $this->currency->equals($money->getCurrency())){
            return false;
        }
        return true;

    }
    public function add(Money $money)
    {
        if (!$this->currency->equals($money->getCurrency())){
            throw new \InvalidArgumentException('Invalid argument');
        }
        $this->amount += $money->getAmount();

    }
}
