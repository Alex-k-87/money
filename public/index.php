<?php

require_once '../vendor/autoload.php';

use \App\Currency;
use \App\Money;

$usd = new Currency\Currency(Currency\Currency::CURRENCY_USD);
$eur = new Currency\Currency(Currency\Currency::CURRENCY_EUR);

$money = new Money\Money(100,$eur);
$money2 = new Money\Money(100,$eur);

//$money->equals($money2);

$money->add($money2);
echo $money->getAmount() . ' ' . $money->getCurrency()->getIsoCode();